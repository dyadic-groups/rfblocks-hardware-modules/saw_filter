# SAW Filter Template

## Features

- Filter package DCC6C.  Suitable for a wide range of filters.
- Dual filter and filter with gain stage board variants.

## Documentation

Full documentation for the filter is available at
[RF Blocks](https://rfblocks.org/boards/SAWFilter-Template.html)

## License

[CERN-OHL-W v2.](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2)
